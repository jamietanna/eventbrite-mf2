package me.jvt.eventbrite.mf2.model.eventbrite;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Venue {

  public Address address;
  public String name;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Address {

    @JsonProperty("address_1")
    public String address1;
    @JsonProperty("address_2")
    public String address2;
    public String city;
    @JsonProperty("postal_code")
    public String postalCode;
    public String country;
    @JsonProperty("localized_address_display")
    public String localizedAddressDisplay;
  }
}
