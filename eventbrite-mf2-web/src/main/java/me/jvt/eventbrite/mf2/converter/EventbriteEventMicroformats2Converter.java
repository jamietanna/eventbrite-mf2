package me.jvt.eventbrite.mf2.converter;

import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import me.jvt.eventbrite.mf2.model.mf2.HEvent;
import me.jvt.eventbrite.mf2.model.mf2.HEvent.Item;
import me.jvt.eventbrite.mf2.model.mf2.HEvent.Item.Properties;
import me.jvt.eventbrite.mf2.model.mf2.HEvent.Location;

import java.util.Optional;

public class EventbriteEventMicroformats2Converter {

  public HEvent convert(Event event, Optional<Venue> maybeVenue) {
    HEvent hEvent = new HEvent();

    hEvent.items = new Item[1];
    hEvent.items[0] = new Item();
    hEvent.items[0].type = singletonArray("h-event");

    hEvent.items[0].properties = new Properties();
    hEvent.items[0].properties.name = singletonArray(event.name.text);

    hEvent.items[0].properties.description = singletonArray(event.summary);

    hEvent.items[0].properties.url = singletonArray(event.url);

    hEvent.items[0].properties.end = singletonArray(event.end.utc);
    hEvent.items[0].properties.start = singletonArray(event.start.utc);

    if (maybeVenue.isPresent()) {
      Venue venue = maybeVenue.get();

      hEvent.items[0].properties.location = new Location[1];
      hEvent.items[0].properties.location[0] = new Location();

      hEvent.items[0].properties.location[0].type = singletonArray("h-adr");
      hEvent.items[0].properties.location[0].properties = new Location.Properties();
      hEvent.items[0].properties.location[0].properties.name = singletonArray(venue.name);
      hEvent.items[0].properties.location[0].properties.locality = singletonArray(venue.address.city);
      hEvent.items[0].properties.location[0].properties.streetAddress = singletonArray(
          buildStreetAddress(venue));
      hEvent.items[0].properties.location[0].properties.postalCode = singletonArray(
          venue.address.postalCode);
    }
    return hEvent;
  }

  private String[] singletonArray(String s) {
    return new String[]{s};
  }

  private String buildStreetAddress(Venue venue) {
    Venue.Address address = venue.address;
    String streetAddress = null;

    if (null == address.address1) {
      streetAddress = address.localizedAddressDisplay;
    } else if (null == address.address2) {
      streetAddress = address.address1;
    } else {
      streetAddress = String.format("%s, %s", venue.address.address1, venue.address.address2);
    }

    return streetAddress;
  }

}
