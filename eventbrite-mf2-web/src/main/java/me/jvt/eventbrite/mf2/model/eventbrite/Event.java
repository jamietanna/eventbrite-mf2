package me.jvt.eventbrite.mf2.model.eventbrite;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {

  public Date end;
  public Name name;
  public String summary;
  public Date start;
  @JsonProperty("venue_id")
  public String venueId;
  public String url;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Name {

    public String text;
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Date {

    public String utc;
  }
}
