package me.jvt.eventbrite.mf2.client;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Optional;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.EventbriteApiErrorResponse;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import me.jvt.eventbrite.mf2.restassured.RequestSpecificationFactory;
import org.apache.http.HttpStatus;

public class EventbriteClientImpl implements EventbriteClient {

  private static final String EVENTBRITE_API_BASEURI = "https://www.eventbriteapi.com/v3";

  private final RequestSpecificationFactory requestSpecificationFactory;
  private final String apiToken;

  public EventbriteClientImpl(RequestSpecificationFactory requestSpecificationFactory,
      String apiToken) {
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.apiToken = apiToken;
  }

  @Override
  public Optional<Event> retrieveEvent(String eventId) throws EventbriteApiException {
    RequestSpecification requestSpecification = this.requestSpecificationFactory
        .newRequestSpecification();
    Response eventbriteApiResponse = requestSpecification.baseUri(EVENTBRITE_API_BASEURI)
        .auth().preemptive()
        .oauth2(this.apiToken)
        .get(String.format("/events/%s", eventId));

    if (HttpStatus.SC_NOT_FOUND == eventbriteApiResponse
        .getStatusCode()) {
      return Optional.empty();
    }

    if (HttpStatus.SC_OK != eventbriteApiResponse
        .getStatusCode()) {
      throw new EventbriteApiException(eventbriteApiResponse.as(
          EventbriteApiErrorResponse.class));
    }

    return Optional.of(eventbriteApiResponse.as(Event.class));
  }

  @Override
  public Optional<Venue> retrieveVenue(String venueId) throws EventbriteApiException {
    RequestSpecification requestSpecification = this.requestSpecificationFactory
        .newRequestSpecification();
    Response eventbriteApiResponse = requestSpecification.baseUri(EVENTBRITE_API_BASEURI)
        .auth().preemptive()
        .oauth2(this.apiToken)
        .get(String.format("/venues/%s", venueId));

    if (HttpStatus.SC_NOT_FOUND == eventbriteApiResponse
        .getStatusCode()) {
      return Optional.empty();
    }

    if (HttpStatus.SC_OK != eventbriteApiResponse
        .getStatusCode()) {
      throw new EventbriteApiException(eventbriteApiResponse.as(
          EventbriteApiErrorResponse.class));
    }

    return Optional.of(eventbriteApiResponse.as(Venue.class));
  }
}
