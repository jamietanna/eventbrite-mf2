package me.jvt.eventbrite.mf2.client;

import java.util.Optional;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;

public interface EventbriteClient {

  Optional<Event> retrieveEvent(String anyString) throws EventbriteApiException;

  Optional<Venue> retrieveVenue(String venueId) throws EventbriteApiException;
}
