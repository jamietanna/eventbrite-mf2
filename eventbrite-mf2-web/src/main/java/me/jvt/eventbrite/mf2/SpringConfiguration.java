package me.jvt.eventbrite.mf2;

import me.jvt.eventbrite.mf2.client.EventbriteClient;
import me.jvt.eventbrite.mf2.client.EventbriteClientImpl;
import me.jvt.eventbrite.mf2.converter.EventbriteEventMicroformats2Converter;
import me.jvt.eventbrite.mf2.restassured.RequestSpecificationFactory;
import me.jvt.eventbrite.mf2.service.EventbriteEventService;
import me.jvt.eventbrite.mf2.service.EventbriteEventServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {

  @Value("${eventbrite.apiToken}")
  private String eventbriteApiToken;

  @Bean("eventbrite.apiToken")
  public String eventbriteApiToken() {
    return eventbriteApiToken;
  }

  @Bean
  public EventbriteClient eventbriteClient(RequestSpecificationFactory requestSpecificationFactory,
      String eventbriteApiToken) {
    return new EventbriteClientImpl(requestSpecificationFactory, eventbriteApiToken);
  }

  @Bean
  public EventbriteEventMicroformats2Converter eventbriteEventMicroformats2Converter() {
    return new EventbriteEventMicroformats2Converter();
  }

  @Bean
  public EventbriteEventService eventbriteEventService(EventbriteClient eventbriteClient) {
    return new EventbriteEventServiceImpl(eventbriteClient);
  }

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }
}
