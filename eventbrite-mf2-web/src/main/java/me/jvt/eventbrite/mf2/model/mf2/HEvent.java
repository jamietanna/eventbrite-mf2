package me.jvt.eventbrite.mf2.model.mf2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HEvent {

  public Item[] items;

  public static class Item {

    public String[] type;
    public Properties properties;

    public static class Properties {

      public String[] name;
      public String[] description;
      public String[] start;
      public String[] end;
      public String[] url;
      @JsonInclude(Include.NON_NULL)
      public Location[] location;
    }
  }

  public static class Location {

    public String[] type;
    public Properties properties;

    public static class Properties {

      @JsonInclude(Include.NON_NULL)
      public String[] name;
      @JsonProperty("street-address")
      public String[] streetAddress;
      public String[] locality;
      @JsonProperty("postal-code")
      @JsonInclude(Include.NON_NULL)
      public String[] postalCode;
      @JsonProperty("country-name")
      @JsonInclude(Include.NON_NULL)
      public String[] countryName;
      @JsonInclude(Include.NON_NULL)
      public String[] url;
    }
  }

}

