package me.jvt.eventbrite.mf2.exception;

import me.jvt.eventbrite.mf2.model.eventbrite.EventbriteApiErrorResponse;

public class EventbriteApiException extends Exception {

  private final int statusCode;
  private final String error;

  public int getStatusCode() {
    return statusCode;
  }

  public String getError() {
    return error;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  private final String errorDescription;

  public EventbriteApiException(int statusCode, String error,
      String errorDescription) {
    super(String.format("Error received from the Eventbrite API, %d (%s): %s", statusCode, error,
        errorDescription));
    this.statusCode = statusCode;
    this.error = error;
    this.errorDescription = errorDescription;
  }

  public EventbriteApiException(EventbriteApiErrorResponse eventbriteApiErrorResponse) {
    this(eventbriteApiErrorResponse.statusCode, eventbriteApiErrorResponse.error,
        eventbriteApiErrorResponse.errorDescription);
  }
  // .class
}
