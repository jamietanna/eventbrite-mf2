package me.jvt.eventbrite.mf2.model.eventbrite;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventbriteApiErrorResponse {

  public String error;
  @JsonProperty("error_description")
  public String errorDescription;
  @JsonProperty("status_code")
  public int statusCode;
}
