package me.jvt.eventbrite.mf2.service;

import java.util.Optional;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;

public interface EventbriteEventService {

  Optional<Event> retrieveEvent(String eventId) throws EventbriteApiException;

  Optional<Venue> retrieveVenue(String venueId) throws EventbriteApiException;
}
