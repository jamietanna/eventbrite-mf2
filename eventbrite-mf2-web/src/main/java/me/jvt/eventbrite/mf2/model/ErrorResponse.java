package me.jvt.eventbrite.mf2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

  public String error;
  @JsonProperty("error_description")
  public String errorDescription;
  @JsonProperty("status_code")
  public int statusCode;
}
