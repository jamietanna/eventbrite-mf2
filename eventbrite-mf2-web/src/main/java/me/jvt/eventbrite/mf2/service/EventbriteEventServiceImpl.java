package me.jvt.eventbrite.mf2.service;

import java.util.Optional;
import me.jvt.eventbrite.mf2.client.EventbriteClient;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;

public class EventbriteEventServiceImpl implements EventbriteEventService {

  private final EventbriteClient eventbriteClient;

  public EventbriteEventServiceImpl(EventbriteClient eventbriteClient) {
    this.eventbriteClient = eventbriteClient;
  }

  public Optional<Event> retrieveEvent(String eventId) throws EventbriteApiException {
    return eventbriteClient.retrieveEvent(eventId);
  }

  @Override
  public Optional<Venue> retrieveVenue(String venueId) throws EventbriteApiException {
    return eventbriteClient.retrieveVenue(venueId);
  }
}
