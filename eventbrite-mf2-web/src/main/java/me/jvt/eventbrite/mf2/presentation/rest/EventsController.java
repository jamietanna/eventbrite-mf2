package me.jvt.eventbrite.mf2.presentation.rest;

import java.util.Optional;
import me.jvt.eventbrite.mf2.converter.EventbriteEventMicroformats2Converter;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import me.jvt.eventbrite.mf2.model.mf2.HEvent;
import me.jvt.eventbrite.mf2.service.EventbriteEventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventsController {

  private final EventbriteEventService eventbriteEventService;
  private final EventbriteEventMicroformats2Converter eventbriteEventMicroformats2Converter;

  public EventsController(EventbriteEventService eventbriteEventService,
      EventbriteEventMicroformats2Converter eventbriteEventMicroformats2Converter) {
    this.eventbriteEventService = eventbriteEventService;
    this.eventbriteEventMicroformats2Converter = eventbriteEventMicroformats2Converter;
  }

  @GetMapping("/e/*-{eventId:[0-9]+}")
  public ResponseEntity<HEvent> getEvent(@PathVariable("eventId") String eventId)
      throws EventbriteApiException {
    Optional<Event> eventbriteEvent = eventbriteEventService
        .retrieveEvent(eventId);
    if (!eventbriteEvent.isPresent()) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    Event actualEvent = eventbriteEvent.get();
    Optional<Venue> eventbriteVenue = eventbriteEventService.retrieveVenue(actualEvent.venueId);

    HEvent hEvent = eventbriteEventMicroformats2Converter
        .convert(actualEvent, eventbriteVenue);

    return new ResponseEntity<>(hEvent, HttpStatus.OK);
  }

}
