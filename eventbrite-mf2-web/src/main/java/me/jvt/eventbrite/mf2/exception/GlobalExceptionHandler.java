package me.jvt.eventbrite.mf2.exception;

import me.jvt.eventbrite.mf2.model.ErrorResponse;
import me.jvt.eventbrite.mf2.model.eventbrite.EventbriteApiErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(EventbriteApiException.class)
  public final ResponseEntity<ErrorResponse> handleEventbriteApiErrorResponse(
      EventbriteApiException eventbriteApiException, WebRequest req) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.statusCode = eventbriteApiException.getStatusCode();
    errorResponse.error = eventbriteApiException.getError();
    errorResponse.errorDescription = eventbriteApiException.getErrorDescription();
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
