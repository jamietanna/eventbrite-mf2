package me.jvt.eventbrite.mf2.converter;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Event.Date;
import me.jvt.eventbrite.mf2.model.eventbrite.Event.Name;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue.Address;
import me.jvt.eventbrite.mf2.model.mf2.HEvent;
import me.jvt.eventbrite.mf2.model.mf2.HEvent.Location;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class EventbriteEventMicroformats2ConverterTest {

  private Event event;
  private Venue venue;

  private EventbriteEventMicroformats2Converter sut;

  @Before
  public void setup() {
    sut = new EventbriteEventMicroformats2Converter();

    event = new Event();
    event.end = new Date();
    event.end.utc = "2019-01-09T22:30:00+01:00";
    event.name = new Name();
    event.name.text = "Name goes here";
    event.summary = "Some description";
    event.start = new Date();
    event.start.utc = "2019-01-09T18:30:00+01:00";
    event.url = "https://foo.bar";

    venue = new Venue();
    venue.address = new Address();
    venue.address.address1 = "10 First Line";
    venue.address.address2 = "Something Street";
    venue.address.city = "Cityville";
    venue.address.country = "GB";
    venue.address.postalCode = "AB1 2ZZ";

    venue.name = "FooBar Co";
  }

  @Test
  public void itConvertsTitle() {
    // given
    event.name.text = "The event name";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].properties.name).containsExactly("The event name");
  }

  @Test
  public void itConvertsSummary() {
    // given
    event.summary = "This is the event";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].properties.description).containsExactly("This is the event");
  }


  @Test
  public void itConvertsUrl() {
    // given
    event.url = "https://www.eventbrite.co.uk/e/beyond-rest-the-future-of-web-apis-tickets-54846386017";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].properties.url).containsExactly(
        "https://www.eventbrite.co.uk/e/beyond-rest-the-future-of-web-apis-tickets-54846386017");
  }

  @Test
  public void itConvertsEndAsUtc() {
    // given
    event.end.utc = "2019-09-09T21:00:00+01:00";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].properties.end).containsExactly("2019-09-09T21:00:00+01:00");
  }

  @Test
  public void itConvertsStartAsUtc() {
    // given
    event.start.utc = "2019-09-09T18:30:00+01:00";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].properties.start).containsExactly("2019-09-09T18:30:00+01:00");
  }

  @Test
  public void itIsAnHevent() {
    // given

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].type).containsExactly("h-event");
  }

  @Test
  public void itConvertsLocationDataAsAnHadr() {
    // given

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];

    assertThat(actualLocation.type).containsExactly("h-adr");
  }

  @Test
  public void itConvertsLocationStreetAddress() {
    // given

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    assertThat(actual.items[0].properties.location).hasSize(1);
    Location actualLocation = actual.items[0].properties.location[0];

    assertThat(actualLocation.type).containsExactly("h-adr");
    assertThat(actualLocation.properties.name).containsExactly("FooBar Co");
    assertThat(actualLocation.properties.streetAddress)
        .containsExactly("10 First Line, Something Street");
    assertThat(actualLocation.properties.locality).containsExactly("Cityville");
  }

  @Test
  public void itConvertsLocationName() {
    // given

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];
    assertThat(actualLocation.properties.name).containsExactly("FooBar Co");
  }

  @Test
  public void itConvertsLocationCity() {
    // given

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];
    assertThat(actualLocation.properties.locality).containsExactly("Cityville");
  }

  @Test
  public void itConvertsPostalCode() {
    // given
    venue.address.postalCode = "NG1 2ZZ";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];
    assertThat(actualLocation.properties.postalCode).containsExactly("NG1 2ZZ");
  }

  @Test
  public void itDoesNotSpecifyTheCountryName() {
    // given

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];
    assertThat(actualLocation.properties.countryName).isNull();
  }

  @Test
  public void itHandlesAddress2AsNull() {
    // given
    venue.address.address2 = null;

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];
    assertThat(actualLocation.properties.streetAddress[0]).doesNotContain("null");
  }

  @Test
  public void itReplaysLocalisedAddressIfBothAddressFieldsAreNull() {
    // given
    venue.address.address1 = null;
    venue.address.address2 = null;
    venue.address.localizedAddressDisplay = "Something, AB1 2EB";

    // when
    HEvent actual = sut.convert(event, Optional.of(venue));

    // then
    Location actualLocation = actual.items[0].properties.location[0];
    assertThat(actualLocation.properties.streetAddress).containsExactly("Something, AB1 2EB");
  }

  @Test
  public void itDoesNotSetLocationIfNoVenue() {
    HEvent actual = sut.convert(event, Optional.empty());

    assertThat(actual.items[0].properties.location).isNull();
  }
}
