package me.jvt.eventbrite.mf2.presentation.rest;

import me.jvt.eventbrite.mf2.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = Application.class)
@AutoConfigureMockMvc
public class ApplicationIntegrationTest {

  @Test
  public void contextLoads() {
    // empty test that would fail if our Spring configuration does not load correctly
  }

}

