package me.jvt.eventbrite.mf2.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import me.jvt.eventbrite.mf2.client.EventbriteClient;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EventbriteEventServiceImplTest {

  @Mock
  private EventbriteClient mockEventbriteClient;

  private EventbriteEventServiceImpl sut;

  @Before
  public void setup() {
    sut = new EventbriteEventServiceImpl(mockEventbriteClient);
  }

  @Test
  public void itDelegatesToClientForEventAndReturnsTheResponseIfNotEmpty() throws Exception {
    // given
    Event mockEvent = mock(Event.class);
    when(mockEventbriteClient.retrieveEvent(anyString())).thenReturn(Optional.of(mockEvent));

    // when
    Optional<Event> actual = sut.retrieveEvent("1234");

    // then
    assertThat(actual).isPresent();
    assertThat(actual.get()).isSameAs(mockEvent);
    verify(mockEventbriteClient).retrieveEvent("1234");
  }

  @Test
  public void itDelegatesToClientForEventAndReturnsTheResponseIfEmpty() throws Exception {
    // given
    Event mockEvent = mock(Event.class);
    when(mockEventbriteClient.retrieveEvent(anyString())).thenReturn(Optional.empty());

    // when
    Optional<Event> actual = sut.retrieveEvent("234567890");

    // then
    assertThat(actual).isEmpty();
    verify(mockEventbriteClient).retrieveEvent("234567890");
  }

  @Test
  public void itDelegatesToClientForVenueAndReturnsTheResponseIfNotEmpty() throws Exception {
    // given
    Venue mockVenue = mock(Venue.class);
    when(mockEventbriteClient.retrieveVenue(anyString())).thenReturn(Optional.of(mockVenue));

    // when
    Optional<Venue> actual = sut.retrieveVenue("abc-123");

    // then
    assertThat(actual).isPresent();
    assertThat(actual.get()).isSameAs(mockVenue);
    verify(mockEventbriteClient).retrieveVenue("abc-123");
  }

  @Test
  public void itDelegatesToClientAndReturnsTheResponseIfEmpty() throws Exception {
    // given
    when(mockEventbriteClient.retrieveVenue(anyString())).thenReturn(Optional.empty());

    // when
    Optional<Venue> actual = sut.retrieveVenue("v1234");

    // then
    assertThat(actual).isEmpty();
    verify(mockEventbriteClient).retrieveVenue("v1234");
  }
}
