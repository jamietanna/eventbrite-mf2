package me.jvt.eventbrite.mf2.presentation.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(RootController.class)
public class RootControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void itReturnsRedirectToArticle() throws Exception {
    // given

    // when
    mockMvc.perform(get("/"))
        // then
        // make this a temporary redirect, so we can change this in the future without risking caching
        .andExpect(MockMvcResultMatchers.status().is(HttpStatus.FOUND.value()))
        .andExpect(
            MockMvcResultMatchers
                .redirectedUrl(
                    "https://www.jvt.me/posts/2019/10/19/microformats-eventbrite/?utm_medium=eventbrite-mf2")
        );
  }
}
