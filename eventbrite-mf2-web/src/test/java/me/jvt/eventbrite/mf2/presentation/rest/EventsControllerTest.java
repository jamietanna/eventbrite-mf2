package me.jvt.eventbrite.mf2.presentation.rest;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Optional;
import me.jvt.eventbrite.mf2.converter.EventbriteEventMicroformats2Converter;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import me.jvt.eventbrite.mf2.model.mf2.HEvent;
import me.jvt.eventbrite.mf2.model.mf2.HEvent.Item;
import me.jvt.eventbrite.mf2.model.mf2.HEvent.Item.Properties;
import me.jvt.eventbrite.mf2.service.EventbriteEventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(EventsController.class)
public class EventsControllerTest {

  @MockBean
  private EventbriteEventMicroformats2Converter mockEventbriteEventMicroformats2Converter;
  @MockBean
  private EventbriteEventService mockEventbriteEventService;

  @Autowired
  private MockMvc mockMvc;

  private Event fakeEvent;
  private HEvent fakeHEvent;
  private Venue fakeVenue;

  @Before
  public void setup() throws Exception {
    fakeEvent = new Event();
    fakeEvent.venueId = "v2345";
    fakeHEvent = new HEvent();
    fakeHEvent.items = new Item[1];
    fakeHEvent.items[0] = new Item();
    fakeHEvent.items[0].properties = new Properties();
    fakeHEvent.items[0].properties.description = new String[]{"some event"};

    fakeVenue = new Venue();

    when(mockEventbriteEventService.retrieveEvent(anyString())).thenReturn(Optional.of(fakeEvent));
    when(mockEventbriteEventService.retrieveVenue(anyString())).thenReturn(Optional.of(fakeVenue));
    when(mockEventbriteEventMicroformats2Converter.convert(any(Event.class), any()))
        .thenReturn(fakeHEvent);
  }

  @Test
  public void receivesRequestForAGivenEventNameAndId() throws Exception {
    // given

    // when
    mockMvc.perform(get("/e/beyond-rest-the-future-of-web-apis-tickets-54846386017"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isOk()
        );
  }

  @Test
  public void receivesRequestForADifferentEventNameAndId() throws Exception {
    // given

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isOk()
        );
  }

  @Test
  public void eventWithNoIdReturnsNotFound() throws Exception {
    // given

    // when
    mockMvc.perform(get("/e/something-new-no-id"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isNotFound()
        );
  }

  @Test
  public void eventbriteEventServiceIsCalledWithTheEventId() throws Exception {
    // given

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isOk()
        );

    verify(mockEventbriteEventService).retrieveEvent("12345");
  }

  @Test
  public void eventbriteEventServiceIsCalledWithTheVenueId() throws Exception {
    // given
    fakeEvent.venueId = "v1234";

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isOk()
        );

    verify(mockEventbriteEventService).retrieveVenue("v1234");
  }

  @Test
  public void eventbriteEventIsConvertedToMicroformats2Response() throws Exception {
    // given

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isOk()
        )
        .andExpect(jsonPath("$.items").isArray())
        .andExpect(jsonPath("$.items").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].properties.description").isArray())
        .andExpect(jsonPath("$.items[0].properties.description[0]").value("some event"));

    verify(mockEventbriteEventMicroformats2Converter).convert(fakeEvent, Optional.of(fakeVenue));
  }

  @Test
  public void notFoundIsReturnedWhenNoEventIsReturnedByService() throws Exception {
    // given
    when(mockEventbriteEventService.retrieveEvent(anyString())).thenReturn(Optional.empty());

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isNotFound()
        );
  }

  @Test
  public void internalServerErrorIsReturnedWhenMeetupApiExceptionThrownForEvent() throws Exception {
    // given
    when(mockEventbriteEventService.retrieveEvent(anyString()))
        .thenThrow(new EventbriteApiException(401, "SOMETHING", "Big description here"));

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isInternalServerError()
        )
        .andExpect(jsonPath("$.status_code").value(401))
        .andExpect(jsonPath("$.error").value("SOMETHING"))
        .andExpect(jsonPath("$.error_description").value("Big description here"));
  }

  @Test
  public void internalServerErrorIsReturnedWhenMeetupApiExceptionThrownForVenue() throws Exception {
    // given
    when(mockEventbriteEventService.retrieveVenue(anyString()))
        .thenThrow(new EventbriteApiException(401, "SOMETHING", "Big description here"));

    // when
    mockMvc.perform(get("/e/something-new-12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.status().isInternalServerError()
        )
        .andExpect(jsonPath("$.status_code").value(401))
        .andExpect(jsonPath("$.error").value("SOMETHING"))
        .andExpect(jsonPath("$.error_description").value("Big description here"));
  }
}
