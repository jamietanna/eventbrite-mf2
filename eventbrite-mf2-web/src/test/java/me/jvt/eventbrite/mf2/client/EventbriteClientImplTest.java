package me.jvt.eventbrite.mf2.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.AuthenticationSpecification;
import io.restassured.specification.PreemptiveAuthSpec;
import io.restassured.specification.RequestSpecification;
import java.util.Optional;
import me.jvt.eventbrite.mf2.exception.EventbriteApiException;
import me.jvt.eventbrite.mf2.model.eventbrite.Event;
import me.jvt.eventbrite.mf2.model.eventbrite.EventbriteApiErrorResponse;
import me.jvt.eventbrite.mf2.model.eventbrite.Venue;
import me.jvt.eventbrite.mf2.restassured.RequestSpecificationFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EventbriteClientImplTest {

  @Mock
  private Event mockEvent;
  @Mock
  private PreemptiveAuthSpec mockPreemptiveAuthSpec;
  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;
  @Mock
  private RequestSpecificationFactory mockRequestSpecificationFactory;
  @Mock
  private Response mockResponse;
  @Mock
  private Venue mockVenue;

  private EventbriteApiErrorResponse fakeEventbriteApiErrorResponse;
  private EventbriteClientImpl sut;

  @Before
  public void setup() {
    fakeEventbriteApiErrorResponse = new EventbriteApiErrorResponse();
    fakeEventbriteApiErrorResponse.error = "NOT_FOUND";
    fakeEventbriteApiErrorResponse.errorDescription = "Something wasn't found because reasons";
    fakeEventbriteApiErrorResponse.statusCode = 404;

    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);
    AuthenticationSpecification mockAuthenticationSpecification = mock(
        AuthenticationSpecification.class);
    when(mockRequestSpecification.auth()).thenReturn(mockAuthenticationSpecification);
    when(mockAuthenticationSpecification.preemptive()).thenReturn(mockPreemptiveAuthSpec);
    when(mockPreemptiveAuthSpec.oauth2(anyString())).thenReturn(mockRequestSpecification);

    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);

    when(mockResponse.as(Event.class)).thenReturn(mockEvent);
    when(mockResponse.as(EventbriteApiErrorResponse.class))
        .thenReturn(fakeEventbriteApiErrorResponse);
    when(mockResponse.as(Venue.class)).thenReturn(mockVenue);
    when(mockResponse.getStatusCode()).thenReturn(200);

    sut = new EventbriteClientImpl(mockRequestSpecificationFactory, "secret-token-here");
  }

  @Test
  public void retrieveEventCallsRetrieveApiWithToken() throws Exception {
    // given

    // when
    sut.retrieveEvent("123");

    // then
    verify(mockRequestSpecification).baseUri("https://www.eventbriteapi.com/v3");
    verify(mockRequestSpecification).get("/events/123");
    verify(mockPreemptiveAuthSpec).oauth2("secret-token-here");
  }

  @Test
  public void retrieveEventCallsRetrieveApiWithTokenAndReturnsEventWhen200OkResponse()
      throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    Optional<Event> actualOptional = sut.retrieveEvent("123");

    // then
    assertThat(actualOptional).isPresent();
    assertThat(actualOptional.get()).isSameAs(mockEvent);
  }

  @Test
  public void retrieveEventCallsRetrieveApiWithTokenAndReturnsEmptyWhenResponseIs404NotFound()
      throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(404);

    // when
    Optional<Event> actualOptional = sut.retrieveEvent("123");

    // then
    assertThat(actualOptional).isEmpty();
  }

  @Test
  public void retrieveEventCallsRetrieveApiThrowsEventbriteApiErrorResponseIfNot200OkOr404NotFound()
      throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(401);

    // when
    assertThatThrownBy(() -> {
      sut.retrieveEvent("234");
    })
        // then
        .isInstanceOf(EventbriteApiException.class)
        .hasMessage(
            "Error received from the Eventbrite API, 404 (NOT_FOUND): Something wasn't found because reasons");
  }

  @Test
  public void retrieveVenueCallsRetrieveApiWithToken() throws Exception {
    // given

    // when
    sut.retrieveVenue("123");

    // then
    verify(mockRequestSpecification).baseUri("https://www.eventbriteapi.com/v3");
    verify(mockRequestSpecification).get("/venues/123");
    verify(mockPreemptiveAuthSpec).oauth2("secret-token-here");
  }

  @Test
  public void retrieveVenueCallsRetrieveApiWithTokenAndReturnsVenueWhen200OkResponse()
      throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    Optional<Venue> actualOptional = sut.retrieveVenue("123");

    // then
    assertThat(actualOptional).isPresent();
    assertThat(actualOptional.get()).isSameAs(mockVenue);
  }

  @Test
  public void retrieveVenueCallsRetrieveApiWithTokenAndReturnsEmptyWhenResponseIs404NotFound()
      throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(404);

    // when
    Optional<Venue> actualOptional = sut.retrieveVenue("234");

    // then
    assertThat(actualOptional).isEmpty();
  }

  @Test
  public void retrieveVenueCallsRetrieveApiThrowsEventbriteApiErrorResponseIfNot200OkOr404NotFound()
      throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(401);

    // when
    assertThatThrownBy(() -> {
      sut.retrieveVenue("234");
    })
        // then
        .isInstanceOf(EventbriteApiException.class)
        .hasMessage(
            "Error received from the Eventbrite API, 404 (NOT_FOUND): Something wasn't found because reasons");
  }
}
