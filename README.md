# eventbrite-mf2

eventbrite-mf2 is an API for Eventbrite.com which produces [Microformats2](https://indieweb.org/microformats) data.

More details [can be found in the announce blog post](https://www.jvt.me/posts/2019/10/19/microformats-eventbrite).

This API is licensed under the GPU Affero General Public License v3, for more details I would recommend [a read of the license's terms on TL;DR Legal](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).

